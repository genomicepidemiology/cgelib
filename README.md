# README #


## Installation

```bash
git clone https://bitbucket.org/genomicepidemiology/cgelib.git
cd cgelib
python -m pip install .

```

## Release

Update version number in the following files:

* setup.cfg
* setup.py
* VERSION

```bash

python -m build
twine upload dist/*

```
