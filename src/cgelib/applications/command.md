
###Testing of Application error
```python3
>>> from command import ApplicationError
>>> err = ApplicationError(1, "helloworld", "", "Some error text")
>>> err.returncode, err.cmd, err.stdout, err.stderr
(1, 'helloworld', '', 'Some error text')
>>> print(err)
Non-zero return code 1 from 'helloworld', message 'Some error text'
>>> noterr = ApplicationError(1, "helloworld", "Some success text", "")
>>> noterr.returncode, noterr.cmd, noterr.stdout, noterr.stderr
(1, 'helloworld', 'Some success text', '')
>>> print(noterr)
Non-zero return code 1 from 'helloworld'

```
